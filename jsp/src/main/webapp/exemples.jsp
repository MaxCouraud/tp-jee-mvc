<%@page pageEncoding="UTF-8" contentType="text/html" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Hello Java EE</title>
  </head>
  <body>
    <p>Hello JSP</p>
    <p>${2 + 5}</p>
    <p>${(2 + 5) < 10} équivalent à ${(2 + 5) lt 10}</p>
    <p>${empty maliste ? "liste vide" : maliste[0]}</p>
    <p>${not empty maliste ? maliste[0] : "liste vide"}</p>
  </body>
</html>
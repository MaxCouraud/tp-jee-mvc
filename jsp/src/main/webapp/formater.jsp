<%@page pageEncoding="UTF-8" contentType="text/html" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <fmt:formatNumber value="${1024 * 1024}"/>
        
        <jsp:useBean id="now" scope="page" class="java.util.Date"/>
 
        
        <p>
        <fmt:formatDate type = "both" dateStyle = "medium" timeStyle = "medium" value="${now}"/>
        </p>
    </body>
</html>
package formulaireMVC;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/formulaire")
public class Formulaire extends HttpServlet {
	
	  @Override
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	                 throws ServletException, IOException {
		  String email = req.getParameter("email");
		  String mdp = req.getParameter("mdp");
		  String mdpverif = req.getParameter("mdpverif");
		  String verfifie = req.getParameter("checkBox");

		  FormulaireValidation form = new FormulaireValidation();
		  
		  try {
			  form.validationEmail( email );
		    } catch ( Exception e ) {
		        form.setErreur( "email", e.getMessage() );
		    }

		    try {
		        form.validationMotsDePasse( mdp, mdpverif );
		    } catch ( Exception e ) {
		        form.setErreur( "mdp", e.getMessage() );
		    }


		    if ( form.getErreurs().isEmpty() ) {
		    	req.setAttribute("email", email);
				  RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/inscrit.jsp");
				  dispatcher.forward(req, resp);
		    } else {
		    	req.setAttribute("erreur", form.getErreurs());
				  RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
				  dispatcher.forward(req, resp);
		    }
		  
	  }

}